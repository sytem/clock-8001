# Websocket-clock

1. Build binary, or download it from http://sytem.fi/clock-8001/websocket-clock.zip

2. Run, if on same computer as companion, need change port either end, eg.

 `./websocket-clock --osc-listen 0.0.0.0:1246 `
 
3. open page in browser, http://localhost:8800/clock.html
 
 most of usual command line arguments will work, all colours not yet implemted
