package main

import (
	"github.com/gorilla/websocket"
	"github.com/jessevdk/go-flags"
	"gitlab.com/Depili/clock-8001/clock"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

var options struct {
	TextRed       uint8 `short:"r" long:"red" description:"Red component of text color" default:"255"`
	TextGreen     uint8 `short:"g" long:"green" description:"Green component of text color" default:"128"`
	TextBlue      uint8 `short:"b" long:"blue" description:"Blue component of text color" default:"0"`
	StaticRed     uint8 `long:"static-red" description:"Red component of static color" default:"80"`
	StaticGreen   uint8 `long:"static-green" description:"Green component of static color" default:"80"`
	StaticBlue    uint8 `long:"static-blue" description:"Blue component of static color" default:"0"`
	SecRed        uint8 `long:"sec-red" description:"Red component of second color" default:"200"`
	SecGreen      uint8 `long:"sec-green" description:"Green component of second color" default:"0"`
	SecBlue       uint8 `long:"sec-blue" description:"Blue component of second color" default:"0"`
	EngineOptions *clock.EngineOptions
}

var parser = flags.NewParser(&options, flags.Default)

var clients = make(map[*websocket.Conn]bool) // connected clients
var broadcast = make(chan Message)           // broadcast channel

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// Message to send on websocket.
type Message struct {
	Leds       int    `json:"leds"`
	Hours      string `json:"hours"`
	Minutes    string `json:"minutes"`
	Seconds    string `json:"seconds"`
	Dots       bool   `json:"dots"`
	Tally      string `json:"tally"`
	TallyRed   uint8  `json:"tally_red"`
	TallyGreen uint8  `json:"tally_green"`
	TallyBlue  uint8  `json:"tally_blue"`
	cd2Red     uint8  `json:"cd2_red"`
	cd2Green   uint8  `json:"cd2_green"`
	cd2Blue    uint8  `json:"cd2_blue"`
	TextRed    uint8  `json:"text_red"`
	TextGreen  uint8  `json:"text_green"`
	TextBlue   uint8  `json:"text_blue"`
}

func main() {
	if _, err := parser.Parse(); err != nil {
		if flagsErr, ok := err.(*flags.Error); ok && flagsErr.Type == flags.ErrHelp {
			os.Exit(0)
		} else {
			os.Exit(1)
		}
	}

	fs := http.FileServer(http.Dir("html"))
	http.Handle("/", fs)

	http.HandleFunc("/ws", handleConnections)
	go handleMessages()

	go tickTime()

	log.Println("http server started on :8800")
	err42 := http.ListenAndServe(":8800", nil)
	if err42 != nil {
		panic(err42)
	}
}

func handleConnections(w http.ResponseWriter, r *http.Request) {
	// Upgrade initial GET request to a websocket
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("New connection \n")
	// Make sure we close the connection when the function returns
	//defer ws.Close()

	// Register our new client
	clients[ws] = true
}

func handleMessages() {
	for {
		// Grab the next message from the broadcast channel
		msg := <-broadcast
		// Send it out to every client that is currently connected
		for client := range clients {
			err := client.WriteJSON(msg)
			if err != nil {
				log.Printf("error: %v", err)
				client.Close()
				delete(clients, client)
			}
		}
	}
}

func tickTime() {
	// Trap SIGINT aka Ctrl-C
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)

	updateTicker := time.NewTicker(time.Millisecond * 30)

	engine, err := clock.MakeEngine(options.EngineOptions)
	if err != nil {
		panic(err)
	}

	log.Printf("Entering main loop\n")
	for {
		select {
		case <-sigChan:
			// SIGINT received, shutdown gracefully
			os.Exit(1)
		case <-updateTicker.C:
			engine.Update()
			var msg Message
			msg.Leds = engine.Leds
			msg.Seconds = engine.Seconds
			msg.Minutes = engine.Minutes
			msg.Hours = engine.Hours

			msg.Dots = engine.Dots
			msg.Tally = engine.Tally

			msg.TallyRed = engine.TallyRed
			msg.TallyGreen = engine.TallyGreen
			msg.TallyBlue = engine.TallyBlue

			//msg.cd2Red = engine.cd2Red
			//msg.cd2Green = engine.cd2Green
			//msg.cd2Blue = engine.cd2Blue

			msg.TextRed = options.TextRed
			msg.TextGreen = options.TextGreen
			msg.TextBlue = options.TextBlue
			broadcast <- msg
		}
	}
}
