// Canvas clock, Teppo Rekola 2019

function runClock() {
  var output = document.getElementById("output");
	var socket = new WebSocket("ws://localhost:8800/ws");

	socket.onopen = function () {
		output.innerHTML += "Status: Connected\n";
	};

  socket.onmessage = function (e) {
		//output.innerHTML = "Server: " + e.data + "\n";
    clock (e.data)
	};
	
  socket.onclose = function(e) {
    output.innerHTML = "Status: connection closed\n";
	clock ("")
    };

}

function clock(msg) {
  var obj = JSON.parse(msg);
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');

  if (window.innerHeight < window.innerWidth ){
    canvas.height = window.innerHeight -20; //to fit without scrollbars
    canvas.width = canvas.height;
  }
  else {
    canvas.height = window.innerWidth -20; //to fit without scrollbars
    canvas.width = canvas.height;
  }

  var radius = canvas.height * 1.5;

  ctx.translate(canvas.width/2, canvas.height/2);   //start at center

  // static marks
  ctx.save();
  ctx.rotate(-Math.PI / 2);
  ctx.strokeStyle = 'yellow';
  ctx.lineWidth = canvas.height / 50;
  ctx.lineCap = 'round';
  for (var i = 0; i < 12; i++) {
    ctx.beginPath();
    ctx.rotate(Math.PI / 6);
    ctx.moveTo(radius/4 + ctx.lineWidth * 2, 0);
    ctx.lineTo(radius/4 + ctx.lineWidth * 2, 0);
    ctx.stroke();
  }
  ctx.restore();

  var sec = obj.seconds;
  var min = obj.minutes;
  var hr = obj.hours;

  // not needed, websocket data is padded
  // format numbers for print
  //if (sec < 10) {sec = "0" + sec};
  //if (min < 10) {min = "0" + min};

  ctx.save();
  // TODO: font, color from data
  var fontSize = canvas.height / 6
  ctx.font = fontSize + "px sans-serif";
  //ctx.font = fontSize + "px Dotrice-Regular,sans-serif"; // why this wont work first frame after reload?
  ctx.textAlign = "center";

  ctx.fillStyle = fullColorHex(obj.text_red,obj.text_green,obj.text_blue);
  if (obj.dots) {
	ctx.fillText(hr + ":" + min, 0, canvas.height / 10);
  }
  else {
	ctx.fillText(hr + " " + min, 0, canvas.height / 10);
  }	
  ctx.fillText(sec, 0, canvas.height / 10 + fontSize);
  
  ctx.fillStyle = fullColorHex(obj.tally_red,obj.tally_green,obj.tally_blue);
  ctx.fillText(obj.tally, 0, canvas.height / 10 - fontSize);
  ctx.restore();

  // Write red dots
  ctx.save();
  ctx.rotate(-Math.PI / 2);

  if (obj.leds < 60){
    for (var i = 0; i <= obj.leds; i++) {
      ctx.strokeStyle = '#FF0000';
      ctx.lineWidth = canvas.height / 50;
      ctx.beginPath();
      ctx.moveTo(radius/4-ctx.lineWidth/8, 0);
      ctx.lineTo(radius/4+ctx.lineWidth/8, 0);
      ctx.lineCap = 'round';
      ctx.stroke();
      ctx.rotate( Math.PI / 30);
    }
  }
  ctx.restore();

}

function fullColorHex(r,g,b) {   
  var red = rgbToHex(r);
  var green = rgbToHex(g);
  var blue = rgbToHex(b);
  return "#"+red+green+blue;
};

function rgbToHex(rgb) { 
  var hex = Number(rgb).toString(16);
  if (hex.length < 2) {
       hex = "0" + hex;
  }
  return hex;
};